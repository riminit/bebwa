const account_service = require("./account.service")

module.exports = {
    generateWallet: async (req, res) => {
        await account_service.generateWallet(async (err, results) => {
            if (err) {
                console.log(err)
                return res.status(500).json({
                    success: false,
                    message: "Server internal error, unable to create wallet."
                })
            } else {
                return res.status(200).json({
                    success: true,
                    message: "Wallet generated successfully.",
                    wallet: {
                        privateKey: results.getPrivateKeyString(),
                        address: results.getAddressString()
                    }
                })
            }
        })
    },
    getETHBal: async (req, res) => {
        await account_service.getETHBal(req.body.address, async (err, results) => {
            if (err) {
                console.log(err)
                return res.status(500).json({
                    success: false,
                    message: "Server internal error, unable to get balance."
                })
            } else {
                return res.status(200).json({
                    success: true,
                    message: "Balance retrieved successfully.",
                    results: results
                })
            }
        })
    },
    getERC20Bal: async (req, res) => {
        await account_service.getERC20Bal(req.body.address, async (err, results) => {
            if (err) {
                console.log(err)
                return res.status(500).json({
                    success: false,
                    message: "Server internal error, unable to get ERC20 token balance."
                })
            } else {
                return res.status(200).json({
                    success: true,
                    message: "Balance retrieved successfully.",
                    results: results
                })
            }
        })
    },
    sendETH: async (req, res) => {
        let body = req.body
        if (!body.sender.address || !body.sender.privateKey ||
            !body.receiver || !body.amount) {
            return res.status(400).json({
                success: false,
                message: "Missing API parameter, please contact support."
            })
        }

        await account_service.sendETH(body, async (err, results) => {
            if (err) {
                console.log(err)
                return res.status(500).json({
                    success: false,
                    message: "Server internal error, unable to send ETH.",
                    error: err
                })
            } else {
                return res.status(200).json({
                    success: true,
                    message: "ETH transaction broadcasted successfully.",
                    results: results
                })
            }
        })
    },
    sendERC20: async (req, res) => {
        let body = req.body
        if (!body.sender.address || !body.sender.privateKey ||
            !body.receiver || !body.amount) {
            return res.status(400).json({
                success: false,
                message: "Missing API parameter, please contact support."
            })
        }

        await account_service.sendERC20(body, async (err, results) => {
            if (err) {
                console.log(err)
                return res.status(500).json({
                    success: false,
                    message: "Server internal error, unable to send ERC20 Token.",
                    error: err
                })
            } else {
                return res.status(200).json({
                    success: true,
                    message: "ERC20 Token transaction broadcasted successfully.",
                    results: results
                })
            }
        })
    },
    isTransactionConfirmed: async (req, res) => {
        await account_service.isTransactionConfirmed(req.body.txnid, async (err, results) => {
            if (err) {
                console.log(err)
                return res.status(500).json({
                    success: false,
                    message: "Server internal error, unable to retrieve status."
                })
            } else {
                return res.status(200).json({
                    success: true,
                    message: "Transaction status retrieved successfully.",
                    results: results
                })
            }
        })
    }
}