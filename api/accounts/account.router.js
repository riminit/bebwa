const account_controller = require("./account.controller")
const router = require("express").Router()

router.get("/generate", account_controller.generateWallet)
router.get("/ethBal", account_controller.getETHBal)
router.get("/erc20Bal", account_controller.getERC20Bal)

router.post("/sendETH", account_controller.sendETH)
router.post("/sendERC20", account_controller.sendERC20)
router.post("/txnStatus", account_controller.isTransactionConfirmed)

module.exports = router