const pool = require("../../config/database")
const ethWallet = require('ethereumjs-wallet')
const Web3 = require("web3")
const EthereumTx = require('ethereumjs-tx').Transaction
const axios = require('axios')
const web3 = new Web3(new Web3.providers.HttpProvider('https://ropsten.infura.io/v3/' + process.env.INFURA_ID))
const web3ws = new Web3(new Web3.providers.WebsocketProvider('wss://ropsten.infura.io/ws/v3/' + process.env.INFURA_ID))
const BigNumber = require("bignumber.js")

// WEENUS ERC20 Token
const tokenAddress = "0x101848D5C5bBca18E6b4431eEdF6B95E9ADF82FA"
const tokenABI = [{ "constant": true, "inputs": [], "name": "name", "outputs": [{ "name": "", "type": "string" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [{ "name": "spender", "type": "address" }, { "name": "tokens", "type": "uint256" }], "name": "approve", "outputs": [{ "name": "success", "type": "bool" }], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "totalSupply", "outputs": [{ "name": "", "type": "uint256" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [{ "name": "from", "type": "address" }, { "name": "to", "type": "address" }, { "name": "tokens", "type": "uint256" }], "name": "transferFrom", "outputs": [{ "name": "success", "type": "bool" }], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "decimals", "outputs": [{ "name": "", "type": "uint8" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [{ "name": "tokenOwner", "type": "address" }], "name": "balanceOf", "outputs": [{ "name": "balance", "type": "uint256" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [], "name": "acceptOwnership", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "owner", "outputs": [{ "name": "", "type": "address" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "symbol", "outputs": [{ "name": "", "type": "string" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [], "name": "drip", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [{ "name": "to", "type": "address" }, { "name": "tokens", "type": "uint256" }], "name": "transfer", "outputs": [{ "name": "success", "type": "bool" }], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [{ "name": "spender", "type": "address" }, { "name": "tokens", "type": "uint256" }, { "name": "data", "type": "bytes" }], "name": "approveAndCall", "outputs": [{ "name": "success", "type": "bool" }], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "newOwner", "outputs": [{ "name": "", "type": "address" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [{ "name": "tokenAddress", "type": "address" }, { "name": "tokens", "type": "uint256" }], "name": "transferAnyERC20Token", "outputs": [{ "name": "success", "type": "bool" }], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [{ "name": "tokenOwner", "type": "address" }, { "name": "spender", "type": "address" }], "name": "allowance", "outputs": [{ "name": "remaining", "type": "uint256" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [{ "name": "_newOwner", "type": "address" }], "name": "transferOwnership", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "inputs": [], "payable": false, "stateMutability": "nonpayable", "type": "constructor" }, { "payable": true, "stateMutability": "payable", "type": "fallback" }, { "anonymous": false, "inputs": [{ "indexed": true, "name": "_from", "type": "address" }, { "indexed": true, "name": "_to", "type": "address" }], "name": "OwnershipTransferred", "type": "event" }, { "anonymous": false, "inputs": [{ "indexed": true, "name": "from", "type": "address" }, { "indexed": true, "name": "to", "type": "address" }, { "indexed": false, "name": "tokens", "type": "uint256" }], "name": "Transfer", "type": "event" }, { "anonymous": false, "inputs": [{ "indexed": true, "name": "tokenOwner", "type": "address" }, { "indexed": true, "name": "spender", "type": "address" }, { "indexed": false, "name": "tokens", "type": "uint256" }], "name": "Approval", "type": "event" }]
const tokenInst = new web3.eth.Contract(tokenABI, tokenAddress)

async function getCurrentGasPrices() {
    let response = await axios.get('https://ethgasstation.info/json/ethgasAPI.json')
    let prices = {
        low: response.data.safeLow / 10,
        medium: response.data.average / 10,
        high: response.data.fast / 10
    }
    return prices
}

async function saveAddress(data) {
    try {
        let label = data.label ? data.label : null
        var results = await pool.query(
            `INSERT INTO Address(address, label)
            VALUES(?, ?)
            ON DUPLICATE KEY UPDATE label=?`,
            [
                data.address,
                label,
                label
            ]
        )
        console.log("-I- Address saved")
    } catch (error) {
        console.log("-E-  saveAddress() error", error)
    }
}

async function saveTransaction(data) {
    let inputs = [null, null]

    if (data.input != "0x") {
        inputs = await web3.eth.abi.decodeParameters(['address', 'uint256'], '0x' + data.input.slice(10))
    }

    try {
        var results = await pool.query(
            `INSERT INTO Transaction(\`from\`, gas, gasPrice, \`hash\`, input, nonce, r, s, \`to\`, v, \`value\`, _value, _to)
            VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`,
            [
                data.from,
                data.gas,
                data.gasPrice,
                data.hash,
                data.input,
                data.nonce,
                data.r,
                data.s,
                data.to,
                data.v,
                data.value,
                inputs[1],
                inputs[0]
            ]
        )
        console.log("-I- Transaction saved")
    } catch (error) {
        console.log("-E-  saveTransaction() error", error)
    }
}

module.exports = {
    generateWallet: async (callback) => {
        try {
            let addressData = await ethWallet.generate()
            console.log(`Private Key = ${addressData.getPrivateKeyString()}`)
            console.log(`Address = ${addressData.getAddressString()}`)

            saveAddress({
                address: addressData.getAddressString()
            })

            return callback(null, addressData)
        } catch (error) {
            return callback(error)
        }
    },
    getETHBal: async (address, callback) => {
        try {
            let cmcAPI = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest?symbol=ETH&convert=USD'
            let response = await axios.get(cmcAPI, {
                headers: {
                    "X-CMC_PRO_API_KEY": process.env.CMC_API_KEY
                }
            })

            let ethQuote = response.data.data.ETH.quote

            return await web3.eth.getBalance(address, async (err, result) => {
                if (err) {
                    return callback(err)
                } else {
                    let usdBal = web3.utils.fromWei(result, "ether") * ethQuote.USD.price
                    return callback(null, usdBal + " USD")
                }
            })
        } catch (error) {
            return callback(error)
        }
    },
    getERC20Bal: async (address, callback) => {
        try {
            let bal = await tokenInst.methods.balanceOf(address).call()
            const decimals = await tokenInst.methods.decimals().call()
            const adjustedBal = new BigNumber(bal).shiftedBy(-Number(decimals))
            return callback(null, adjustedBal)

        } catch (error) {
            return callback(error)
        }
    },
    sendETH: async (data, callback) => {
        try {
            let sender = data.sender
            var nonce = await web3.eth.getTransactionCount(sender.address)

            web3.eth.getBalance(sender.address, async (err, result) => {
                if (err) {
                    return callback(err)
                } else {
                    let balance = web3.utils.fromWei(result, "ether")

                    if (balance < data.amount) {
                        return callback('Insufficient funds.')
                    } else if (data.amount <= 0) {
                        return callback("Can't send 0 or negative amount of ETH.")
                    } else {
                        let gasPrices = await getCurrentGasPrices()

                        let gas = await web3.eth.estimateGas({
                            "from": sender.address,
                            "nonce": nonce,
                            "to": data.receiver,
                            "chainId": 3
                        })

                        let details = {
                            "to": data.receiver,
                            "value": web3.utils.toHex(web3.utils.toWei(data.amount.toString(), 'ether')),
                            "gas": gas * 1.5,
                            "gasPrice": gasPrices.medium * 1000000000,
                            "nonce": nonce,
                            "chainId": 3 // EIP 155 chainId - mainnet: 1, rinkeby: 4, ropsten: 3
                        }

                        const transaction = new EthereumTx(details, { chain: 'ropsten' })
                        let privateKey = sender.privateKey.split('0x')
                        let privKey = Buffer.from(privateKey[1], 'hex')
                        transaction.sign(privKey)

                        const serializedTransaction = transaction.serialize()

                        await web3.eth.sendSignedTransaction('0x' + serializedTransaction.toString('hex'), async (err, id) => {
                            if (err) {
                                return callback(err)
                            } else {
                                let tx = await web3.eth.getTransaction(id)
                                saveTransaction(tx)

                                const url = `https://ropsten.etherscan.io/tx/${id}`
                                return callback(null, { id: id, link: url })
                            }
                        })
                    }
                }
            })
        } catch (error) {
            return callback(error)
        }
    },
    sendERC20: async (data, callback) => {
        try {
            let sender = data.sender
            var nonce = await web3.eth.getTransactionCount(sender.address)

            tokenInst.methods.balanceOf(sender.address).call().then(async (result) => {
                let balance = web3.utils.fromWei(result, "ether")
                if (balance < data.amount) {
                    return callback('Insufficient funds.')
                } else if (data.amount <= 0) {
                    return callback("Can't send 0 or negative amount of tokens.")
                } else {
                    let gasPrices = await getCurrentGasPrices()
                    let amount = web3.utils.toHex(web3.utils.toWei(data.amount.toString(), 'ether'))

                    let gas = await web3.eth.estimateGas({
                        "from": sender.address,
                        "nonce": nonce,
                        "to": tokenAddress,
                        "data": tokenInst.methods.transfer(data.receiver, amount).encodeABI(),
                        "chainId": 3
                    })

                    let details = {
                        "from": sender.address,
                        "to": tokenAddress,
                        "value": "0x0",
                        "gas": gas * 1.5,
                        "gasPrice": gasPrices.low * 1000000000,
                        "nonce": nonce,
                        "data": tokenInst.methods.transfer(data.receiver, amount).encodeABI(),
                        "chainId": 3 // EIP 155 chainId - mainnet: 1, rinkeby: 4, ropsten: 3
                    }

                    const transaction = new EthereumTx(details, { chain: 'ropsten' })
                    let privateKey = sender.privateKey.split('0x')
                    let privKey = Buffer.from(privateKey[1], 'hex')
                    transaction.sign(privKey)

                    const serializedTransaction = transaction.serialize()

                    await web3.eth.sendSignedTransaction('0x' + serializedTransaction.toString('hex'), async (err, id) => {
                        if (err) {
                            return callback(err)
                        } else {
                            let tx = await web3.eth.getTransaction(id)
                            saveTransaction(tx)

                            const url = `https://ropsten.etherscan.io/tx/${id}`
                            return callback(null, { id: id, link: url })
                        }
                    })
                }
            })
        } catch (error) {
            return callback(error)
        }
    },
    isTransactionConfirmed: async (txnid, callback) => {
        try {
            return await web3.eth.getTransactionReceipt(txnid, async (err, result) => {
                if (err) {
                    return callback(err)
                } else {
                    return callback(null, result.status ? "Transaction is confirmed." : "Transaction is not yet confirmed.")
                }
            })
        } catch (error) {
            return callback(error)
        }
    }
}