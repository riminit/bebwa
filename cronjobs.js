const cron = require('node-cron')
const pool = require("./config/database")
const Web3 = require("web3")
const web3 = new Web3(new Web3.providers.HttpProvider('https://ropsten.infura.io/v3/' + process.env.INFURA_ID))

cron.schedule('* * * * *', async () => {
    try {
        let txs = await pool.query(
            `SELECT \`hash\`
            FROM Transaction
            WHERE isSuccess=0`
        )

        txs = JSON.parse(JSON.stringify(txs))

        txs.forEach(async (tx) => {
            var receipt = await web3.eth.getTransactionReceipt(tx.hash)
            if (receipt != null && receipt.status) {
                var results = await pool.query(
                    `UPDATE Transaction
                    SET isSuccess=1, blockHash=?, blockNumber=?, transactionIndex=?
                    WHERE \`hash\`=?`,
                    [
                        receipt.blockHash,
                        receipt.blockNumber,
                        receipt.transactionIndex,
                        tx.hash
                    ]
                )
                console.log("-I- Transaction updated.")
            }
        })
    } catch (error) {
        console.log("-E- Cronjob error", error)
    }
})