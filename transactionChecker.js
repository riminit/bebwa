require("dotenv").config()
const Web3 = require('web3')
const axios = require('axios')

const web3 = new Web3(new Web3.providers.HttpProvider('https://ropsten.infura.io/v3/' + process.env.INFURA_ID))
const web3ws = new Web3(new Web3.providers.WebsocketProvider('wss://ropsten.infura.io/ws/v3/' + process.env.INFURA_ID))

// WEENUS ERC20 Token
const tokenAddress = "0x101848D5C5bBca18E6b4431eEdF6B95E9ADF82FA"
const tokenABI = [{ "constant": true, "inputs": [], "name": "name", "outputs": [{ "name": "", "type": "string" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [{ "name": "spender", "type": "address" }, { "name": "tokens", "type": "uint256" }], "name": "approve", "outputs": [{ "name": "success", "type": "bool" }], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "totalSupply", "outputs": [{ "name": "", "type": "uint256" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [{ "name": "from", "type": "address" }, { "name": "to", "type": "address" }, { "name": "tokens", "type": "uint256" }], "name": "transferFrom", "outputs": [{ "name": "success", "type": "bool" }], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "decimals", "outputs": [{ "name": "", "type": "uint8" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [{ "name": "tokenOwner", "type": "address" }], "name": "balanceOf", "outputs": [{ "name": "balance", "type": "uint256" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [], "name": "acceptOwnership", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "owner", "outputs": [{ "name": "", "type": "address" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "symbol", "outputs": [{ "name": "", "type": "string" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [], "name": "drip", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [{ "name": "to", "type": "address" }, { "name": "tokens", "type": "uint256" }], "name": "transfer", "outputs": [{ "name": "success", "type": "bool" }], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [{ "name": "spender", "type": "address" }, { "name": "tokens", "type": "uint256" }, { "name": "data", "type": "bytes" }], "name": "approveAndCall", "outputs": [{ "name": "success", "type": "bool" }], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "newOwner", "outputs": [{ "name": "", "type": "address" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [{ "name": "tokenAddress", "type": "address" }, { "name": "tokens", "type": "uint256" }], "name": "transferAnyERC20Token", "outputs": [{ "name": "success", "type": "bool" }], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [{ "name": "tokenOwner", "type": "address" }, { "name": "spender", "type": "address" }], "name": "allowance", "outputs": [{ "name": "remaining", "type": "uint256" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [{ "name": "_newOwner", "type": "address" }], "name": "transferOwnership", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "inputs": [], "payable": false, "stateMutability": "nonpayable", "type": "constructor" }, { "payable": true, "stateMutability": "payable", "type": "fallback" }, { "anonymous": false, "inputs": [{ "indexed": true, "name": "_from", "type": "address" }, { "indexed": true, "name": "_to", "type": "address" }], "name": "OwnershipTransferred", "type": "event" }, { "anonymous": false, "inputs": [{ "indexed": true, "name": "from", "type": "address" }, { "indexed": true, "name": "to", "type": "address" }, { "indexed": false, "name": "tokens", "type": "uint256" }], "name": "Transfer", "type": "event" }, { "anonymous": false, "inputs": [{ "indexed": true, "name": "tokenOwner", "type": "address" }, { "indexed": true, "name": "spender", "type": "address" }, { "indexed": false, "name": "tokens", "type": "uint256" }], "name": "Approval", "type": "event" }]
const tokenInst = new web3.eth.Contract(tokenABI, tokenAddress)

async function getCurrentGasPrices() {
    let response = await axios.get('https://ethgasstation.info/json/ethgasAPI.json')
    let prices = {
        low: response.data.safeLow / 10,
        medium: response.data.average / 10,
        high: response.data.fast / 10
    }
    return prices
}

class TransactionChecker {
    constructor(account, tokenAddress) {
        this.account = account.toLowerCase()
        this.tokenAddress = tokenAddress.toLowerCase()
    }

    subscribe(contract, eventName) {
        const eventJsonInterface = web3.utils._.find(
            contract._jsonInterface,
            o => o.name === eventName && o.type === 'event',
        )

        this.subscription = web3ws.eth.subscribe('logs', {
            address: contract.options.address,
            topics: [eventJsonInterface.signature]
        }, (err, res) => {
            if (err) {
                console.log(err)
            }
        }).on('connected', async () => {
            console.log('-I- Watching all ERC20 Weenus Token transfers...')
        })
    }

    watchTransactions() {
        this.subscription.on('data', (log) => {
            setTimeout(async () => {
                try {
                    let tx = await web3.eth.getTransaction(log.transactionHash)

                    if ((tx != null) && (tx.to != null)) {
                        if (tx.to.toLowerCase() == this.tokenAddress) {
                            let results = web3.eth.abi.decodeParameters(['address', 'uint256'], '0x' + tx.input.slice(10))
                            let amount = web3.utils.fromWei(results[1].toString(), 'ether')

                            if (results[0].toLowerCase() == this.account) {
                                let gas = await web3.eth.estimateGas({
                                    "from": process.env.WARM_ADDRESS,
                                    "to": process.env.COLD_ADDRESS,
                                    "data": tokenInst.methods.transfer(process.env.COLD_ADDRESS, web3.utils.toHex(results[1])).encodeABI(),
                                    "chainId": 3
                                })
                                let gasPrices = await getCurrentGasPrices()
                                let txnFee = gasPrices.medium * 1000000000 * gas * 2

                                let api = "http://" + process.env.APP_HOST + "/api/account/sendETH"
                                await axios.post(api, {
                                    sender: {
                                        address: process.env.HOT_ADDRESS,
                                        privateKey: process.env.HOT_PASS
                                    },
                                    receiver: process.env.WARM_ADDRESS,
                                    amount: web3.utils.fromWei(txnFee + "", "ether")
                                }).then(async (res) => {
                                    console.log(res)
                                    if (res.data.success) {
                                        api = "http://" + process.env.APP_HOST + "/api/account/sendERC20"
                                        await axios.post(api, {
                                            sender: {
                                                address: process.env.WARM_ADDRESS,
                                                privateKey: process.env.WARM_PASS
                                            },
                                            receiver: process.env.COLD_ADDRESS,
                                            amount: amount
                                        }).then((res) => {
                                            console.log(res)
                                        })
                                    }
                                })
                            }
                        }
                    }
                } catch (err) {
                    console.error(err)
                }
            }, 10000)
        })
    }
}

let txChecker = new TransactionChecker(process.env.WARM_ADDRESS, tokenAddress)
txChecker.subscribe(tokenInst, 'Transfer')
txChecker.watchTransactions()