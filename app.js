require("dotenv").config()
require("./cronjobs")

const express = require("express")
const app = express()
const accountRouter = require("./api/accounts/account.router")

app.use(express.json())

app.use("/api/account", accountRouter)
app.listen(process.env.APP_PORT, () => {
    console.log("Server up and running on PORT : ", process.env.APP_PORT)
})